import React, { useCallback, useEffect, useRef, useState } from 'react';
import './App.css';

function App() {
  const [isClosed, setIsClosed] = useState(true);
  const [status, setStatus] = useState(false);
  const [message, setMessage] = useState('');
  const [data, setData] = useState<string[]>([]);
  const myWs = useRef<WebSocket | null>(null);
  const [myID, setMyID] = useState<number>(); // just here because i'm lazy for something else

  useEffect(() => {
    if (!isClosed) {
      myWs.current = new WebSocket('ws://localhost:9000');
      // myWs.current = new WebSocket('ws://192.168.3.111:8089/ws');
      myWs.current.onopen = () => {
        setStatus(true);
        console.log("Соединение открыто");
      }
      myWs.current.onclose = () => {
        setIsClosed(true);
        setStatus(false);
        console.log("Соединение закрыто");
      }

      // обработчик сообщений от сервера
      myWs.current.onmessage = (message: any) => {
        const jsonMessage = JSON.parse(message.data);
        switch (jsonMessage.action) {
          case 'OPEN':
            setMyID(jsonMessage.data.id);
            console.log('setted id', jsonMessage.data.id);
            break;
          case 'MESSAGE': {
            setData((prevState) => [`${jsonMessage.data.id}: ${jsonMessage.data.text}`, ...prevState]);
            console.log('Message: %s', jsonMessage.data.text);
            break;
          }
          default:
            console.log('unknown action');
            break;
        }
      }
    }
    return () => myWs.current?.close(); // кода меняется isClosed - соединение закрывается
  }, [myWs, isClosed]);

  // функция для отправки echo-сообщений на сервер
  const wsSendEcho = useCallback((value: string) => {
    myWs.current?.send(JSON.stringify({action: 'ECHO', data: { id: myID ,text: value.toString()}}));
  }, [myID]);

  // функция для отправки команды ping на сервер
  const wsSendPing = useCallback(() => {
    myWs.current?.send(JSON.stringify({action: 'PING', data: {id: myID}}));
  }, [myID]);

  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '10px', padding: '10px'}}>
      <h2>
        A la CHAT
      </h2>

      <span>
        {status ? 'Online' : 'Offline'}
        <span style={{ display: 'inline-block', width: '10px', height: '10px', background: isClosed ? '#eeb4b4': '#cefabe' }}></span>
      </span>

      <div style={{ display: 'flex', gap: '10px'}}>
        <button onClick={() => setIsClosed(!isClosed)}>
          {!isClosed ? 'Остановить соединение' : 'Открыть соединение' }
        </button>
        <button onClick={() => {
          wsSendPing();
          // setData((prevState) => ['Я: PING', ...prevState]);
        }}>
          отправить пунь
        </button>
      </div>

      <div style={{ maxHeight: '50vh', overflowY: 'scroll', display: 'flex', flexDirection: 'column', background: '#f1fcff', border: '1px solid #95e5c0', padding: '10px' }}>

        <div style={{ display: 'flex', gap: '10px'}}>
          <input type="text" value={message} onChange={(event) => setMessage(event.target.value)}/>
          <button onClick={() => {
            wsSendEcho(message);
            // setData((prevState) => [ 'Я: ' + message, ...prevState]);
            setMessage('');
          }}>
            отправить message
          </button>
        </div>
        {data.map((element) => <p key={`${new Date().toString()} ${Math.random() * 1000}`}>{element}</p>)}
      </div>
    </div>
  )
}

export default App;
