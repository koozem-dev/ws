const WebSocket = require('ws');
const wsServer = new WebSocket.Server({ port: 9000 });

let lastID = 0;
const usersId = [];
let clients = [];

wsServer.on('connection', onConnect);

function onConnect(wsClient) {
  console.log('Новый пользователь');
  wsClient.send(JSON.stringify(
      {
        action: 'OPEN',
        data: {id: lastID},
      })
  );
  usersId.push(lastID);
  clients.push(wsClient);
  lastID += 1;

  wsClient.on('close', function() {
    clients = clients.filter((cc) => cc !== wsClient);
    console.log('Пользователь отключился');
  });

  wsClient.on('message', function(message) {
    try {
      const jsonMessage = JSON.parse(message);
      if (!usersId.some(id => id === jsonMessage.data.id)) {
        throw Error('no user');
      }
      switch (jsonMessage.action) {
        case 'ECHO':
          wsClient.send(JSON.stringify({action: 'MESSAGE', data: jsonMessage.data}));
          clients.filter(cc => cc !== wsClient).forEach(
              cc => cc.send(JSON.stringify({action: 'MESSAGE', data: jsonMessage.data}))
          )
          break;
        case 'PING':
          setTimeout(function() {
            wsClient.send(JSON.stringify({action: 'MESSAGE', data: { id: jsonMessage.data.id, text: 'PONG'}}));
          }, 2000);
          break;
        default:
          console.log('Неизвестная команда');
          break;
      }
    } catch (error) {
      console.log('Ошибка', error);
    }
  });
}

console.log('Сервер запущен на 9000 порту');